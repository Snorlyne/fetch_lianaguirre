import type IUsuario from '@/Interfaces/IUsuario'

const UsuariosData: IUsuario[] =
[
    {
        id: 1,
        email: 'user@gmail.com',
        password: 'User@2024',
        name: 'User',
        age: 19,
    },
    {
        id: 2,
        email: 'user2@gmail.com',
        password: 'User2@2024',
        name: 'Usuario 2',
        age: 20,
    }

]
export default UsuariosData